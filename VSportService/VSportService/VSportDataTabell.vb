Imports System.IO
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib
Imports System.Collections.Specialized


Public Class VSportDataTabell
    Const SPORT_TABELL_LINJE_START = "�"
    Const KODE_INFO_LINJE = "I"
    Const KODE_N_LINJE = "N"
    Const KODE_RUNDE_KAMPER = "A"
    Const KODE_UTSATT_KAMPER = "B"

    Const UNDER_KAT_FOTBALL = "fotball"
    Const UNDER_KAT_HANDBALL = "handball"
    Const UNDER_KAT_ISHOCKEY = "Ishockey"
    Friend Const UNDER_KAT_ISHOCKEY_NORSK_ELITE = "Ishockey_elite_norsk"
    Const UNDER_KAT_VOLLEYBALL = "volleyball"
    Const UNDER_KAT_BANDY = "bandy"
    Const UNDER_KAT_BASKETBALL = "basketball"
    Const UNDER_KAT_BORDTENNIS = "bordtennis"

    Const BRACKET_CODE_LOOKUP_DELIM = "="
    Const BRACKET_VALUES_DELIM = ","
    Private vsp_fil As New ArrayList
    Private ant_linjer As Integer
    Private neste_linjenr As Integer 'holde orden p� hvilken linje av data som er hentet ut
    Private under_kat_sport As String



    Public Sub New(ByVal VSport_fil As ArrayList, ByVal UnderKatSport As String)
        'UnderKatSport sier noe om hvilke "klammeparantes" koder (eks: [t01u2]) som skal brukes. Varierer noe med hvilken sport

        Dim innhold() As String
        'Dim tilde_1_funnet As Boolean = False
        'Dim tilde_2_funnet As Boolean = False

        under_kat_sport = UnderKatSport

        ant_linjer = 0
        'Bygge opp array som skal inneholde kun aktuelle verdier ut fra fil innhold:
        For Each linje As String In VSport_fil
            If linje.StartsWith(SPORT_TABELL_LINJE_START) Then 'En linje VSPort tabell linje som skal tas vare p�:
                innhold = Split(linje, ";", 2)
                If innhold.GetUpperBound(0) = 1 Then 'Best�r av en bokstavkode og noe data:
                    innhold(0) = Replace(innhold(0), SPORT_TABELL_LINJE_START, "") 'Fjerne � tegnet
                    vsp_fil.Add(innhold)
                    ant_linjer += 1
                End If
            End If
        Next



    End Sub


    Private Function Neste_linje() As String()
        Dim s() As String

        s = vsp_fil(neste_linjenr)
        neste_linjenr += 1
        Return s

    End Function

    Public Function Antall_linjer() As Integer
        Return ant_linjer
    End Function


    Public Function Rundens_kamper() As ArrayList

        Return get_linjer(KODE_RUNDE_KAMPER)

    End Function

    Public Function Utsatte_kamper() As ArrayList

        Return get_linjer(KODE_UTSATT_KAMPER)

    End Function

    Public Function Info_linje() As String

        Return get_linje(KODE_INFO_LINJE)

    End Function

    Public Function N_Linje() As String

        Return get_linje(KODE_N_LINJE)

    End Function

    Private Function get_linje(ByVal key As String) As String
        For ii As Integer = 0 To vsp_fil.Count
            If vsp_fil.Item(ii)(0) = key Then Return vsp_fil.Item(ii)(1)
        Next
    End Function


    Private Function get_linjer(ByVal key As String) As ArrayList
        Dim ss As New ArrayList

        For ii As Integer = 0 To vsp_fil.Count - 1
            If vsp_fil.Item(ii)(0) = key Then
                ss.Add(vsp_fil.Item(ii)(1))
            End If
        Next

        Return ss
    End Function



    Public Function GetConvertedSportsTable() As String
        'Konvertere selve datatabellene som er i fila n�r kommer fra VSport, til tabellformat som brukes i Notabene.
        'Returnerer som en string
        'UnderKatSport sier noe om hvilke "klammeparantes" koder (eks: [t01u2]) som skal brukes. Varierer noe med hvilken sport

        Dim linje_nr As Integer
        Dim ny_tab As String
        Dim linje() As String
        Dim tabell As String
        Dim klamme_rundetab As String
        Dim klamme_poengtab As String
        Dim klamme_hoved As String = AppSettings("FIXEDBracketCode_Main")
        Dim klamme_hoved_sub As String = AppSettings("FIXEDBracketCode_MainSub")
        Dim klamme_utsatt As String = AppSettings("FIXEDBracketCode_DelayedMatch")

        Dim bracket_codes() As String = GetBracketCodes(AppSettings("Bracket_codesPath"), under_kat_sport)
        If (Not IsNothing(bracket_codes)) Then 'bracket_codes er array:
            If bracket_codes.GetUpperBound(0) = 1 Then
                klamme_rundetab = bracket_codes(0)
                klamme_poengtab = bracket_codes(1)
            Else
                klamme_rundetab = "[MANGLER TAB-KODE!]"
                klamme_poengtab = "[MANGLER TAB-KODE!]"

            End If
        Else
            klamme_rundetab = "[MANGLER TAB-KODE!]"
            klamme_poengtab = "[MANGLER TAB-KODE!]"

        End If


        'G� igjennom FIP header og sette inn nye koder
        linje_nr = 0
        tabell = "</head>" + vbCrLf + "<body lang=NO-BOK>" + vbCrLf

        While linje_nr < Me.Antall_linjer()
            linje = Me.Neste_linje()
            Select Case linje(0)
                Case "I" 'Infolinje
                    tabell += "<p class=Infolinje>" + linje(1) + vbCrLf

                Case "N" '
                    tabell += "<p class=Tabellkode>" + klamme_hoved + vbCrLf
                    tabell += "<p class=Tabellkode>" + klamme_hoved_sub + vbCrLf
                    tabell += "<p class=Brdtekst><table border=1 cellspacing=0 cellpadding=5><tr><td align=""left"">" + vbCrLf
                    tabell += linje(1) + vbCrLf + "</td></tr></table>" + vbCrLf

                Case "R" 'R (Resultatliste ?? Ser ikke ut til � bli brukt
                    'tabell += "<p class=Brdtekst><table border=1 cellspacing=0 cellpadding=5><tr><td align=""left"">"
                    'tabell += linje(1) + vbCrLf + "</td>" + vbCrLf


                Case "V" 'Start for noen tabeller. Rundens kamper (V=1), utsatte kamper (V=3) etc:
                    If linje(1) = "1" Then 'Start rundens kamper:
                        tabell += "<p class=Tabellkode>" + klamme_rundetab + vbCrLf
                        tabell += "<p class=Brdtekst><table border=1 cellspacing=0 cellpadding=5>" + vbCrLf
                    ElseIf linje(1) = "3" Then
                        tabell += "<p class=Tabellkode>" + klamme_utsatt + vbCrLf
                        tabell += "<p class=Brdtekst><table border=1 cellspacing=0 cellpadding=5>" + vbCrLf

                    End If

                Case "A" 'Rundens kamper:
                    tabell += "<tr>" + formater_rk(linje(1)) + "</tr>" + vbCrLf

                Case "B" 'Utsatte/forsinkede kamper:
                    tabell += "<tr>" + formater_utsatte(linje(1)) + "</tr>" + vbCrLf

                Case "W" 'Stopp for noen tabeller. Rundens kamper (V=1), utsatte kamper (V=3) etc:
                    tabell += "</table>" + vbCrLf


                Case "Z" 'Start/slutt av Poengtabell
                    If linje(1) = "tstart" Then 'Start av T-tabell ((resultattabell):
                        tabell += "<p class=Tabellkode>" + klamme_poengtab + vbCrLf
                        tabell += "<p class=Brdtekst><table border=1 cellspacing=0 cellpadding=5>" + vbCrLf
                    Else 'Avslutte tabell:
                        tabell += "</table>" + vbCrLf
                    End If

                Case "T" 'Resultattabell linje
                    tabell += "<tr>" + formater_poengtabell(linje(1)) + "</tr>" + vbCrLf

                Case "P" 'GEnerell linje (?)
                    tabell += "<p class=Brdtekst>" + linje(1) + vbCrLf

                Case Else
                    tabell += "<p class=Brdtekst><b> UKJENT KODE: " + linje(0) + "</b>Verdi: " + linje(1) + vbCrLf
            End Select
            linje_nr += 1
        End While

        Return tabell
    End Function

    Private Function formater_rk(ByVal semikolon_txt As String) As String
        'Konvertere tekst til et pent format for Rundens kamp:
        'Oppdatert 10.05.2006: Det passet ikke kunder av NTB at det var space rundt strek (-)
        Dim biter() As String
        Dim re As String

        biter = Split(semikolon_txt, ";")

        Select Case biter.GetUpperBound(0)
            Case 1
                're = "<td>" + biter(0) + "&nbsp;-&nbsp;" + biter(1) + "</td>"
                re = "<td>" + biter(0) + "-" + biter(1) + "</td>"
            Case 3
                're = "<td>" + biter(0) + "&nbsp;-&nbsp;" + biter(1) + "</td><td>" _
                '    + biter(2) + "</td><td>-</td><td>" + biter(3) + "</td>"
                re = "<td>" + biter(0) + "-" + biter(1) + "</td><td>" _
                    + biter(2) + "</td><td>-</td><td>" + biter(3) + "</td>"
            Case Else 'Skal ikke forekomme:
                re = "<td>" + semikolon_txt + "</td>"
        End Select

        Return re

    End Function

    Private Function formater_utsatte(ByVal semikolon_txt As String) As String
        'Konvertere tekst til et pent format for Utsatte kamper kamp:
        'Oppdatert 10.05.2006: Det passet ikke kunder av NTB at det var space rundt strek (-)

        Dim biter() As String
        Dim re As String

        biter = Split(semikolon_txt, ";")

        Select Case biter.GetUpperBound(0)
            Case 2
                're = "<td>" + biter(0) + "&nbsp;-&nbsp;" + biter(1) + "&nbsp;&nbsp;" + biter(2) + "</td>"
                re = "<td>" + biter(0) + "-" + biter(1) + "&nbsp;&nbsp;" + biter(2) + "</td>"
            Case Else 'Skal ikke forekomme:
                re = "<td>" + semikolon_txt + "</td>"
        End Select

        Return re
    End Function

    Private Function formater_poengtabell(ByVal semikolon_txt As String) As String
        'Konvertere tekst til et pent format for poengtabell
        Dim biter() As String
        Dim re As String
        Dim cols As Integer
        Dim td_str As String = "<td  align=""left"">"
        Dim td_num As String = "<td align=""right"">"
        Dim sl_td As String = "</td>" + td_num

        biter = Split(semikolon_txt, ";")
        cols = biter.GetUpperBound(0)

        Select Case under_kat_sport
            Case UNDER_KAT_VOLLEYBALL 'De 7 siste kolonnene:
                If cols = 17 Then
                    re = td_str + biter(0) + sl_td + biter(11) _
                        + sl_td + biter(12) + sl_td + biter(13) _
                        + sl_td + biter(14) + sl_td + biter(15) _
                        + sl_td + biter(16) + sl_td + biter(17) + "</td>"

                Else
                    re = "<td><b>UVENTET ANTALL KOLONNER (" & cols & ") </b>" & semikolon_txt & "</td>"
                End If
            Case UNDER_KAT_BASKETBALL
                If cols = 22 Then
                    re = td_str + biter(0) + sl_td + biter(15) _
                        + sl_td + biter(16) + sl_td + biter(18) _
                        + sl_td + biter(19) + sl_td + biter(20) _
                        + sl_td + biter(21) + sl_td + biter(22) + "</td>"

                Else
                    re = "<td><b>UVENTET ANTALL KOLONNER (" & cols & ") </b>" & semikolon_txt & "</td>"
                End If

            Case UNDER_KAT_ISHOCKEY_NORSK_ELITE 'De 9 siste kolonnene:
                'Hvis antall kolonner er 23, antas det at det er Norsk eliteserie, som skal behandles spes.
                If cols = 23 Then 'Ishockey Norsk Eliteserie (hente ut 9 siste kolonner):
                    re = td_str + biter(0) + sl_td + biter(15) _
                        + sl_td + biter(16) + sl_td + biter(17) _
                        + sl_td + biter(18) + sl_td + biter(19) _
                        + sl_td + biter(20) + sl_td + biter(21) _
                        + sl_td + biter(22) + sl_td + biter(23) + "</td>"
                Else
                    re = "<td><b>UVENTET ANTALL KOLONNER (" & cols & ") </b>" & semikolon_txt & "</td>"
                End If

            Case Else
                If cols = 22 Then 'Hente ut 8 siste kolonner:
                    re = td_str + biter(0) + sl_td + biter(15) _
                        + sl_td + biter(16) + sl_td + biter(17) _
                        + sl_td + biter(18) + sl_td + biter(19) _
                        + sl_td + biter(20) + sl_td + biter(21) _
                        + sl_td + biter(22) + "</td>"

                Else
                    re = "<td><b>UVENTET ANTALL KOLONNER (" & cols & ") </b>" & semikolon_txt & "</td>"
                End If

        End Select

        Return re
    End Function


    Private Function GetBracketCodes(ByVal FullPath As String, ByVal sportsgren As String, Optional ByRef ErrInfo As String = "") As String()
        'Hente Klammeparantes koder fordelt p� de ulike sportsgrener:
        Dim kode() As String
        Dim klamme_koder() As String
        Dim aline As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath, System.Text.Encoding.Default)

            'aline = objReader.ReadLine()
            While objReader.Peek <> -1
                aline = objReader.ReadLine()
                If aline.ToCharArray(0, 1) <> ";" And aline.ToCharArray(0, 1) <> " " Then 'Ikke kommentar eller tom linje
                    kode = aline.Split(VSport.CODE_LOOKUP_DELIM)
                    If kode.GetUpperBound(0) = 1 Then
                        If kode(0).ToLower = sportsgren.ToLower Then
                            klamme_koder = Split(kode(1), BRACKET_VALUES_DELIM, 2)
                        End If
                    End If
                End If
            End While
            objReader.Close()
            Return klamme_koder

        Catch ex As Exception
            LogFile.WriteErr(AppSettings("LogPath"), "Feil oppsto i GetBracketCodes.", ex)
        End Try
    End Function

End Class
