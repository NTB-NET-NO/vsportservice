'************************
'File: VSportService.vb
'Date: 23.05.2005
'Date: 21.12.2005 ofn
'************************
Imports System.ServiceProcess
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib
Imports System.IO
Imports System.text
Imports System.Xml
Imports System.Web.Mail
Imports System.Collections.Specialized



Public Class VSport
    Inherits System.ServiceProcess.ServiceBase

    Const TILDE = "~"
    Const FIP_HEADER_DELIM = ":"
    Friend Const CODE_LOOKUP_DELIM = "=" 'Brukes i andre klasser tilh�rende prosjektet
    Const UNDER_KAT_SPORT = "LX" ' Den kode i Vsportfila som forteller hvilken sportsgren dete er (Fotball, bandy...)
    Const SUBJECT_HEADER = "Subject" 'Hvis denne header skal konverteres (ska det vanligvis), sjekk om det skal legges til en PRE tekst (se var "PreSubjectTekst")


#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New VSport}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    Friend WithEvents FileSystemWatcher As System.IO.FileSystemWatcher
    Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.FileSystemWatcher = New System.IO.FileSystemWatcher
        Me.PollTimer = New System.Timers.Timer
        CType(Me.FileSystemWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'FileSystemWatcher
        '
        Me.FileSystemWatcher.EnableRaisingEvents = True
        '
        'PollTimer
        '
        Me.PollTimer.Enabled = True
        Me.PollTimer.Interval = 1000
        '
        'Trav
        '
        Me.ServiceName = "VSportService"
        CType(Me.FileSystemWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Declare Variables
    Private LogPath As String
    Private inputPath As String
    Private outputPath As String
    Private donePath As String
    Private errorPath As String
    Private VSportSTYLE_path As String
    Private VSportHTML_pre_path As String
    Private FIPHeader_codes_path As String
    Private FIXEDHeadersPath As String
    Private PreSubjectTekst As String

    Private pollInterval As Integer
    Private fileTimer As Integer

    Private Style As String

    Dim ok As Boolean = True

    'Warnings
    Dim warnEmail As Boolean
    Dim emailRcpt As String
    Dim SMTPServer As String


    'Class initalisation
    '*******************
    Protected Sub Init()
        'Get global variables from configuration file
        logPath = AppSettings("LogPath")
        inputPath = AppSettings("InputPath")
        outputPath = AppSettings("OutputPath")
        donePath = AppSettings("DonePath")
        errorPath = AppSettings("ErrorPath")
        VSportHTML_pre_path = AppSettings("VSportHTML_prePath")
        VSportSTYLE_path = AppSettings("VSportSTYLEPath")
        FIPHeader_codes_path = AppSettings("FIPHeader_codesPath")
        FIXEDHeadersPath = AppSettings("FIXEDHeadersPath")

        PreSubjectTekst = AppSettings("PreSubjectTekst")

        'create paths
        LogFile.MakePath(LogPath)
        LogFile.MakePath(inputPath)
        LogFile.MakePath(outputPath)
        LogFile.MakePath(donePath)
        LogFile.MakePath(errorPath)

        'Write to logfile
        LogFile.WriteLogNoDate(logPath, "---------------------------------------------------------------------------------")
        LogFile.WriteLog(logPath, "VSport Converter initialiserer...")

        pollInterval = AppSettings("PollingInterval")
        fileTimer = AppSettings("FileTimer")

        'Warning settings
        If (AppSettings("warnEmail") = "True") Then warnEmail = True Else warnEmail = False
        emailRcpt = AppSettings("emailRcpt")
        SMTPServer = AppSettings("SMTPServer")


    End Sub


    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Init()

        PollTimer.Enabled = True
        FileSystemWatcher.Path = inputPath
        FileSystemWatcher.EnableRaisingEvents = True

        LogFile.WriteLog(logPath, "VSport Converter startet." _
            & vbCrLf & " -Ser etter filer i '" + AppSettings("InputPath") + "'" _
            & vbCrLf & " -Legger filer i '" + AppSettings("OutputPath") + "'")
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        PollTimer.Enabled = False
        FileSystemWatcher.EnableRaisingEvents = False

        LogFile.WriteLog(logPath, "VSport Converter stoppet.")
    End Sub

    'Loop through all files
    'in input path
    '**********************
    Private Sub DoAllFiles()

        'Wait for files to be written
        Threading.Thread.Sleep(fileTimer * 1000)

        Dim file As String
        Dim files As String() = Directory.GetFiles(inputPath)

        'Loop the files
        For Each file In files
            DoOneFile(file)
            Threading.Thread.Sleep(500)
        Next
    End Sub



    Private Function MakeOutputFileName(ByVal path As String, ByVal sportsgren As String)
        'Lage filnavn ut fra tekster fra app.config, sportsgren og tidspunkt
        Dim of As String

        of = path & "\" _
            & IIf(AppSettings("OutputFileNamePrefix") <> "", AppSettings("OutputFileNamePrefix") & "_", "") _
            & sportsgren & "_" _
            & Year(Now()) _
            & Month(Now()).ToString.PadLeft(2, "0") _
            & Microsoft.VisualBasic.DateAndTime.Day(Now()).ToString.PadLeft(2, "0") _
            & "_" _
            & Hour(Now()).ToString.PadLeft(2, "0") _
            & Minute(Now()).ToString.PadLeft(2, "0") _
            & Second(Now()).ToString.PadLeft(2, "0") _
            & "." & AppSettings("OutputFileNameExtention")

        'Denne setning vil kunne f�re til at Catch fra den som kaller blir fyrt
        of = System.IO.Path.GetFullPath(of)

        Return of

    End Function



    'Go thorugh given file and convert it using appropriate class
    '***********************************************************
    Private Sub DoOneFile(ByVal filename As String)

        Dim oldfile As String = filename
        Dim outfile As String
        Dim nitf As String
        Dim type As String
        Dim file_content As ArrayList
        Dim strTmp, msg As String
        Dim num As Integer
        Dim ny_fil As String
        Dim UnderKatSport As String


        Try
            'HEnte filinnhold
            file_content = GetFileContents(filename)
            ny_fil = ""
            Dim fip_header As StringDictionary = GetFIPHeader(file_content)

            'M� sjekke om Norsk Eliteserie i Ishockey. Den skal behandles annerledes enn annnen Ishockey.
            ' Derfor lage egen underkategori her (i programmet):
            If (fip_header.Item("LS").ToLower = "ishockey-elite" Or _
                fip_header.Item("LS").ToLower = "ishock-kval-elite" Or _
                fip_header.Item("LS").ToLower = "ishockey-kval-elite" Or _
                fip_header.Item("LS").ToLower = "ishockey-eliteserien") And _
                fip_header.Item("LA").StartsWith("Norsk") Then
                UnderKatSport = VSportDataTabell.UNDER_KAT_ISHOCKEY_NORSK_ELITE
            Else
                UnderKatSport = fip_header.Item(UNDER_KAT_SPORT)
            End If

            outfile = MakeOutputFileName(outputPath, UnderKatSport)


            ny_fil += GetHTMLPre(VSportHTML_pre_path)
            ny_fil += ConvertFIPHeader(fip_header)
            ny_fil += GetStyleDef(VSportSTYLE_path)
            ny_fil += ConvertTables(file_content, UnderKatSport)


            Dim sw As StreamWriter = New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
            sw.WriteLine(ny_fil)
            sw.Close()


        Catch ex As Exception
            msg = ex.Message
            Try
                File.Copy(oldfile, errorPath & "\" & Path.GetFileName(oldfile), True)
                File.Delete(oldfile)
            Catch
            End Try

            LogFile.WriteErr(LogPath, "Konvertering feilet " & type & ": '" & oldfile & "'", ex)
            ok = False

        End Try


        'Copy/delete file
        If ok Then
            'LogFile.WriteLog(logPath, "VSport dokument konvertert" & type & ": '" & filename & "'")

            Try
                Dim donefile As String = donePath & "\" & Path.GetFileName(oldfile)
                donefile = FuncLib.MakeSubDirDate(donefile, File.GetLastWriteTime(oldfile))

                File.Copy(oldfile, donefile, True)
                File.Delete(oldfile)

                LogFile.WriteLog(LogPath, "Slettet: '" & oldfile & "'")
            Catch ex As Exception
                LogFile.WriteErr(LogPath, "Sletting feilet: '" & oldfile & "'", ex)
            End Try
        End If

        If Not ok Then
            'Email
            If warnEmail Then
                WarnOnEmail(msg)
            End If
        End If

    End Sub


    Private Function GetFIPHeader(ByVal file_content As ArrayList) As StringDictionary
        'Ekstraherer tilde-header (FIP-header) fra VSport fila.
        Dim linje() As String
        Dim FIPHeader As New StringDictionary
        Dim linje_nr As Integer
        Dim forste_tilde_funnet As Boolean = False

        'F�rst finne f�rste tilde (~):
        linje_nr = 0
        Try
            While linje_nr <= file_content.Count And Not forste_tilde_funnet
                If file_content(linje_nr) = TILDE Then 'Funnet start av "tildeheader" (f�rste tilde funnet)
                    forste_tilde_funnet = True
                    linje_nr += 1 '�ke til linjen ETTER tilde.
                    While linje_nr <= file_content.Count And file_content(linje_nr) <> TILDE
                        linje = Split(file_content(linje_nr), FIP_HEADER_DELIM, 2)
                        If Not FIPHeader.ContainsKey(linje(0)) Then 'Er ikke lagt til fra f�r
                            FIPHeader.Add(linje(0), linje(1)) 'Legge til hhv. header-kode og -verdi
                        End If
                        linje_nr += 1
                    End While
                Else
                    linje_nr += 1 'Fant ikke f�rste tilde, se etter tilde p� neste linje
                End If
            End While

        Catch ex As Exception
            Dim msg As String = ex.Message
            LogFile.WriteErr(LogPath, "Henting av FIP header feilet (GetFIPHeader): ", ex)
            'ok = False
        End Try

        Return FIPHeader
    End Function


    Private Function ConvertFIPHeader(ByVal fip_header As StringDictionary) As String
        'Konvertere fra kodeverk som er i fila n�r kommer fra VSport, til koder som brukes i Notabene.
        'Returnerer som en string
        Dim linje_nr As Integer
        Dim ntb_kode As String
        'Dim ntb_value As String

        Dim CDP As New CustomDocPropBuilder

        Dim FIP_lookup As StringDictionary = GetFIPCodeLookup(FIPHeader_codes_path)
        'G� igjennom FIP header og sette inn nye koder
        linje_nr = 0

        For Each kode As DictionaryEntry In fip_header
            If FIP_lookup.ContainsKey(kode.Key()) Then
                ntb_kode = FIP_lookup.Item(kode.Key())
                kode.Value() = IIf(ntb_kode = SUBJECT_HEADER, PreSubjectTekst + kode.Value(), kode.Value())

                CDP.AddCustomDocumentProperty(FIP_lookup.Item(kode.Key()), kode.Value())
            End If
        Next

        'Legge til nye 
        linje_nr = 0
        'Dim fixed_headers As DictionaryEntry = GetFixedHeaders(FIXEDHeadersPath)
        For Each linje As DictionaryEntry In GetFixedHeaders(FIXEDHeadersPath)

            CDP.AddCustomDocumentProperty(linje.Key(), linje.Value())
        Next

        CDP.SluttCustomDocumentProperties() 'Legge til slutttag

        Return CDP.GetCustomDocumentPropertiesBlock

    End Function


    Public Function GetFIPCodeLookup(ByVal FullPath As String, Optional ByRef ErrInfo As String = "") As StringDictionary
        'Hente overgangstabell for tilde-koder som kommer med fila eksportert fra VSport, til koder som brukes i Notabene.
        Dim strContents As String
        Dim ii As Integer
        Dim kode() As String
        Dim aline As String
        Dim lines As New StringDictionary
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath, System.Text.Encoding.Default)

            'aline = objReader.ReadLine()
            While objReader.Peek <> -1
                aline = objReader.ReadLine()
                If aline.ToCharArray(0, 1) <> ";" And aline.ToCharArray(0, 1) <> " " Then 'Ikke kommentar eller tom linje
                    kode = Split(aline, CODE_LOOKUP_DELIM, 2)
                    If Not lines.ContainsKey(kode(0)) Then 'Ikke lagt til fra f�r:
                        lines.Add(kode(0), kode(1))
                    End If
                End If
            End While
            objReader.Close()
            Return lines

        Catch ex As Exception
            LogFile.WriteErr(LogPath, "Feil oppsto i GetFIPCodeLookup", ex)
        End Try
    End Function


    'Reading file content
    Public Function GetFileContents(ByVal FullPath As String, Optional ByRef ErrInfo As String = "") As ArrayList

        Dim strContents As String
        Dim ii As Integer
        Dim aline As String
        Dim lines As New ArrayList
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath, System.Text.Encoding.Default)

            'aline = objReader.ReadLine()
            While objReader.Peek <> -1
                aline = objReader.ReadLine()
                If aline <> "" Then lines.Add(aline)
                ii += 1
            End While
            objReader.Close()
            Return lines

        Catch ex As Exception
            LogFile.WriteErr(LogPath, "Feil oppsto i GetFileContents.", ex)
        End Try
    End Function


    Public Function GetFixedHeaders(ByVal FullPath As String, Optional ByRef ErrInfo As String = "") As Hashtable
        'Hente overgangstabell for tilde-koder som kommer med fila eksportert fra VSport, til koder som brukes i Notabene.
        Dim kode() As String
        Dim aline As String
        Dim lines As New Hashtable
        Dim objReader As StreamReader

        Try
            objReader = New StreamReader(FullPath, System.Text.Encoding.Default)

            'aline = objReader.ReadLine()
            While objReader.Peek <> -1
                aline = objReader.ReadLine()
                If aline.ToCharArray(0, 1) <> ";" And aline.ToCharArray(0, 1) <> " " Then 'Ikke kommentar eller tom linje
                    kode = Split(aline, CODE_LOOKUP_DELIM, 2)
                    If kode.GetUpperBound(0) = 1 And Not lines.ContainsKey(kode(0)) Then 'NotaBeneKode og verdi finnes:
                        lines.Add(kode(0), kode(1))
                    End If
                End If
            End While
            objReader.Close()
            Return lines

        Catch ex As Exception
            LogFile.WriteErr(LogPath, "Feil oppsto i GetFixedHeaders.", ex)
        End Try
    End Function





    Public Function GetHTMLPre(ByVal FullPath As String, Optional ByRef ErrInfo As String = "") As String
        'Bygger opp start tagger
        Dim strContents As String
        Dim ii As Integer
        Dim stuff As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath, System.Text.Encoding.Default)

            'aline = objReader.ReadLine()
            stuff = objReader.ReadToEnd()
            objReader.Close()

            Return stuff

        Catch ex As Exception
            LogFile.WriteErr(LogPath, "Feil oppsto i GetHTMLPre.", ex)
        End Try
    End Function



    Public Function GetStyleDef(ByVal FullPath As String, Optional ByRef ErrInfo As String = "") As String
        'Henter inn styledef. fra configfil
        Dim strContents As String
        Dim ii As Integer
        Dim stuff As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath, System.Text.Encoding.Default)

            'aline = objReader.ReadLine()
            stuff = objReader.ReadToEnd()
            objReader.Close()

            Return stuff

        Catch ex As Exception
            LogFile.WriteErr(LogPath, "Feil oppsto i GetStyleDef.", ex)
        End Try
    End Function


    Private Function ConvertTables(ByVal file_content As ArrayList, ByVal UnderKatSport As String) As String
        'Konvertere selve datatabellene som er i fila n�r kommer fra VSport, til tabellformat som brukes i Notabene.
        'Returnerer som en string

        Dim linje_nr As Integer
        Dim ny_tab As String
        Dim linje() As String
        Dim tabell As String

        Dim sport_tabell = New VSportDataTabell(file_content, UnderKatSport)

        tabell = sport_tabell.GetConvertedSportsTable()

        Return tabell

    End Function


    'When timer has counted down, trigger events for timer
    '*************************************************************************************************************************
    Private Sub FileSystemWatcher_Created(ByVal sender As Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher.Created
        'Stop events while handling files
        PollTimer.Enabled = False
        FileSystemWatcher.EnableRaisingEvents = False

        'Do waiting files
        DoAllFiles()

        'Restart events
        PollTimer.Enabled = True
        FileSystemWatcher.EnableRaisingEvents = True
    End Sub


    'Warns on connection errors by e-Mail
    Sub WarnOnEmail(ByVal message As String)
        Dim epost As MailMessage = New MailMessage
        SmtpMail.SmtpServer = SMTPServer

        epost.From = "505@ntb.no"
        epost.To = emailRcpt
        epost.Subject = "Feil i VSportService!"
        epost.BodyFormat = Web.Mail.MailFormat.Text
        epost.Body = "Det har oppst�tt en feil i VSportService." & vbCrLf & vbCrLf
        epost.Body = epost.Body & "Tid: " & Now & vbCrLf
        epost.Body = epost.Body & "Feilmelding: " & message & vbCrLf & vbCrLf
        'epost.Body = epost.Body & query
        Try
            SmtpMail.Send(epost)
        Catch e As Exception
            'Log warning error
            'systemLog.WriteLine(Now & " : FAILED to send warning by e-Mail!")
            'LogError(e)
            LogFile.WriteLog(LogPath, "Feilet ved sending av EPost!")
            LogFile.WriteErr(LogPath, "", e)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        'Stop events while handling files
        PollTimer.Enabled = False
        FileSystemWatcher.EnableRaisingEvents = False

        'Do waiting files
        DoAllFiles()

        'Restart events
        PollTimer.Interval = pollInterval * 1000
        PollTimer.Enabled = True
        FileSystemWatcher.EnableRaisingEvents = True
    End Sub


End Class
