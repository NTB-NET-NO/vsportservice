Imports System.IO
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib

'Verkt�yklasse for HTML og XML stoff.
Public Class CDPBuilder
    Const CDP_START As String = "<XML>" + vbCrLf + "<o:CustomDocumentProperties>" + vbCrLf
    Const CDP_SLUTT As String = "</o:CustomDocumentProperties>" + vbCrLf + "</XML>" + vbCrLf

    Dim cdp_block As String


    Public Sub New()
        StartCustomDocumentProperties()
    End Sub

    Public Sub StartCustomDocumentProperties()
        cdp_block = CDP_START
    End Sub


    Public Sub AddCustomDocumentProperty(ByVal navn As String, ByVal verdi As String)
        cdp_block += "<o:" + navn + " dt:dt=""String"">" + verdi + "</o:" + navn + ">" + vbCrLf
    End Sub


    Public Function SluttCustomDocumentProperties()
        cdp_block += CDP_SLUTT
    End Function

    Public Function GetCustomDocumentPropertiesBlock() As String
        Return cdp_block
    End Function

    Protected Overrides Sub Finalize()
        SluttCustomDocumentProperties()
        MyBase.Finalize()
    End Sub
End Class





